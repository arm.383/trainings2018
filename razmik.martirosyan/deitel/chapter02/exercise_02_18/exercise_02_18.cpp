#include <iostream>

int
main()
{
    int number1, number2;
    std::cout << "Enter the number 1: ";
    std::cin >> number1;
    std::cout << "Enter the number 2: ";
    std::cin >> number2;
    if (number1 > number2) {
        std::cout << "More " << number1 << std::endl;
	return 0;
    }
    if (number1 < number2) {
        std::cout << "More " << number2 << std::endl;
	return 0;
    }
    std::cout << "These numbers are equal" << std::endl;
    return 0;
}
