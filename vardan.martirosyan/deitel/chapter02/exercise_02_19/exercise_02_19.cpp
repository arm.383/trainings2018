#include <iostream>

int 
main()
{
   int number1, number2, number3, sum, average, product, min, max;
  
    std::cout << "Enter three numbers: ";
    std::cin >> number1 >> number2 >> number3;
  
    sum = number1 + number2 + number3;
    std::cout << "Sum " << sum  << std::endl;
  
    product = number1 * number2 * number3;
    std::cout << "Product " << product << std::endl;
  
    average = sum / 3;
    std::cout << "Average " << average << std::endl;
 
    max = number1, min = number1;

    if (number2 > max )
        max = number2;          
    if(number3 > max)
        max = number3;

    if (number2 < min)
        min = number2;
    if (number3 < min)
        min = number3;   
  
    std::cout << max << " Is Biggest " << std::endl;
    std::cout << min << " Is Smallest " << std::endl;
 
    return 0;
}

